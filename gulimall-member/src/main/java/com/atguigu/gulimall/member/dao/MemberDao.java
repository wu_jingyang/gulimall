package com.atguigu.gulimall.member.dao;

import com.atguigu.gulimall.member.entity.MemberEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员
 * 
 * @author Wujingyang
 * @email 954397384@qq.com
 * @date 2020-07-19 12:36:19
 */
@Mapper
public interface MemberDao extends BaseMapper<MemberEntity> {
	
}
