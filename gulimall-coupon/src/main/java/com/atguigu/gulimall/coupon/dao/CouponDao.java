package com.atguigu.gulimall.coupon.dao;

import com.atguigu.gulimall.coupon.entity.CouponEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券信息
 * 
 * @author Wujingyang
 * @email 954397384@qq.com
 * @date 2020-07-17 02:27:21
 */
@Mapper
public interface CouponDao extends BaseMapper<CouponEntity> {
	
}
