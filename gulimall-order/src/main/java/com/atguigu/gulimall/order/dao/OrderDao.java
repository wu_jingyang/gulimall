package com.atguigu.gulimall.order.dao;

import com.atguigu.gulimall.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 * 
 * @author Wujingyang
 * @email 954397384@qq.com
 * @date 2020-07-17 03:16:30
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {
	
}
