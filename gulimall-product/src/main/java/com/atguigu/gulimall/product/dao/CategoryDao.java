package com.atguigu.gulimall.product.dao;

import com.atguigu.gulimall.product.entity.CategoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品三级分类
 * 
 * @author Wujingyang
 * @email 954397384@qq.com
 * @date 2020-07-14 17:56:14
 */
@Mapper
public interface CategoryDao extends BaseMapper<CategoryEntity> {
	
}
