package com.atguigu.gulimall.product.dao;

import com.atguigu.gulimall.product.entity.BrandEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 品牌
 * 
 * @author Wujingyang
 * @email 954397384@qq.com
 * @date 2020-07-14 17:56:14
 */
@Mapper
public interface BrandDao extends BaseMapper<BrandEntity> {
	
}
