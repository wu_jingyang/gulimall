package com.atguigu.gulimall.product;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 1.整合mybatis-plus
 *  1)导入依赖
 *      <dependency>
 *           <groupId>com.baomidou</groupId>
 *           <artifactId>mybatis-plus</artifactId>
 *           <version>3.3.1</version>
 *       </dependency>
 *   2)配置
 *      1.配置数据源
 *          1).配置数据库驱动
 *          2).在application.yml配置数据源
 *      2.配置mybatis-plus
 *          1).使用MqpperScan
 *          2).告诉Mybatis-plus,sql映射文件都在那里
 */
@MapperScan("com.atguigu.gulimall.product.dao")
@SpringBootApplication
public class GulimallProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(GulimallProductApplication.class, args);
    }

}
