package com.atguigu.gulimall.ware.dao;

import com.atguigu.gulimall.ware.entity.PurchaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 * 
 * @author Wujingyang
 * @email 954397384@qq.com
 * @date 2020-07-17 11:42:56
 */
@Mapper
public interface PurchaseDao extends BaseMapper<PurchaseEntity> {
	
}
